import hashlib
import secrets


def create_hash(text: str) -> str:
    return hashlib.md5(text.encode()).hexdigest()

def create_greenhouse_token():
    return secrets.token_hex(20)
