import network
from time import sleep

SSID = "13373-IoT-Lab"
PASSWORD = "FetMc5Un>2dYzEM"

def connect():
    wlan = network.WLAN(network.STA_IF)
    if wlan.isconnected():
        print(wlan.ifconfig())
        return
    
    wlan.active(True)
    wlan.connect(SSID, PASSWORD)

    while wlan.isconnected() == False:
        print('Waiting for connection...')
        sleep(1)
        
    print(wlan.ifconfig())
