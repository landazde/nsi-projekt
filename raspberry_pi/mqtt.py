import connect
from simple import MQTTClient

class MQTT:
    
    def __init__(self):
        connect.connect()
        MQTT_BROKER = "broker.mqtt.cool"
        MQTT_PORT = 1883
        
        self.client = MQTTClient(
            client_id = "Test",
            server=MQTT_BROKER,
            port=MQTT_PORT
            )


    def publish(self, value):  
        self.client.publish(
            "meassurements",
            value
            )

