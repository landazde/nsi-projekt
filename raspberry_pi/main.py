from machine import Pin, Timer
import sys
import random
import settings
import com_send
from mqtt import MQTT
import utime


def send_all(tmr):
    temperature = random.uniform(-50,50)		# Generování hodnoty
    ph = random.uniform(0,14)
    moisture = random.uniform(10,45)
    value = "{};{};{:.1f};{:.1f};{}".format(settings.USERNAME,
                                settings.GREENHOUSE_ID,
                                temperature,
                                ph,
                                moisture,
                                settings.TOKEN
                                )	# Formátování hodnoty
    com_send.send(value) 
    global mqtt
    mqtt.publish(value)

    
# Funkce zalévání na GPIO
def watering(topic, message):
    pin = Pin(0,Pin.IN)
    pin.value(1)
    print("Zalévání...")
    utime.sleep(1)
    pin.value(0)
    
    
machine.Pin('LED',Pin.OUT).value(0)
mqtt = MQTT()
mqtt.client.set_callback(watering)
mqtt.client.connect()
topic = 'watering/' + str(settings.GREENHOUSE_ID)
mqtt.client.subscribe(topic.encode('utf-8'))

timer = Timer()
timer.init(period=20000, callback=send_all)

while True:
    mqtt.client.check_msg()
    utime.sleep(1)

