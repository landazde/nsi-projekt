import sqlite3
import threading


class DataController:
    file: str
    connection: sqlite3.Connection
    cursor: sqlite3.Cursor
    lock = threading.Lock()

    tablename: str


    def __init__(self, file: str, username: str, greenhouseId: int):
        self.file = file
        self.connection = sqlite3.connect(file, check_same_thread=False)
        self.cursor = self.connection.cursor()
        self.tablename = username + "_" + str(greenhouseId)
        self.greenhouseId = greenhouseId
        self._create_table_if_not_exists()


    def _create_table_if_not_exists(self) -> None:
        query = f"""
        CREATE TABLE IF NOT EXISTS {self.tablename}(
            timestamp TEXT NOT NULL,
            temp REAL NOT NULL,
            pH REAL NOT NULL,
            moisture INTEGER NOT NULL
        );"""

        self.lock.acquire()

        self.cursor.execute(query)
        self.connection.commit()

        self.lock.release()



    def add_value(self, timestamp: str, temperature: float, pH: float, moisture: int) -> None:
        query = f"""
        INSERT INTO {self.tablename} (timestamp,temp,pH,moisture)
        VALUES (?,{temperature},{pH},{moisture});
        """
        print(self.tablename)
        self.lock.acquire()

        self.cursor.execute(query,[timestamp])
        self.connection.commit()

        self.lock.release()



    def select_x_last(self, x: int) -> list:
        query = f"""
        SELECT * FROM {self.tablename}
        ORDER BY ROWID DESC LIMIT {x};
        """

        self.lock.acquire()

        self.cursor.execute(query)
        result = self.cursor.fetchall()

        self.lock.release()
        return result
    

    def delete_y_last(self, y: int) -> None:
        query = f"""
        DELETE FROM {self.tablename}
        WHERE ROWID == {y};
        """
        self.lock.acquire()

        self.cursor.execute(query)
        self.connection.commit()

        self.lock.release()



    def close_connection(self):
        self.lock.acquire()
        self.connection.close()
        self.lock.release()
