from flask import request, Response, redirect
from datetime import datetime
from flask import Blueprint
import json
import static
from hash import create_hash
import jwt
from datetime import datetime
import generate_values



app = Blueprint("api", __name__)



@app.route("/api/showLast", methods=["GET"])
def get_last_x():
    """
    Returns last x inserted values
    """
    try:
        token = request.args.get("token")
        x = int(request.args.get("x"))
        greenhouseId = int(request.args.get("greenhouseId"))
    except:
        return Response(status=206)

    if x == None or token == None:
        return Response(status=206)

    payload = jwt.decode(token, static.jwt_secret, algorithms=static.jwt_algorithm)
    
    username = payload.get("username")
    validity = payload.get("validity")
    random_payload = payload.get("rnd_payload")
    if username == None or validity == None or random_payload == None:
        return redirect("/login", code=302)

    result = static.get_data_controller(username, greenhouseId)
    if isinstance(result, Response):
        return result

    values = result.select_x_last(x)
    return Response(status=200, mimetype="application/json", response=json.dumps(values))



@app.route("/api/deleteLast", methods=["PATCH"])
def delete_last():
    """
    Deletes last y inserted values
    """
    try:
        token = request.args.get("token")
        y = int(request.args.get("y"))
        greenhouseId = int(request.args.get("greenhouseId"))
    except:
        return Response(status=206)

    if y == None or token == None:
        return Response(status=206)

    payload = jwt.decode(token, static.jwt_secret, algorithms=static.jwt_algorithm)
    
    username = payload.get("username")
    validity = payload.get("validity")
    random_payload = payload.get("rnd_payload")
    if username == None or validity == None or random_payload == None:
        return redirect("/login", code=302)
    

    result = static.get_data_controller(username, greenhouseId)
    if isinstance(result, Response):
        return result
        
    result.delete_y_last(y)
    return Response(status=200, mimetype='application/json')



@app.route("/api/getToken", methods=["POST"])
def get_token():
    """
    Creates and returns json web token
    """ 
    # Loading json body
    if request.headers.get("Content-Type") == "application/json":
        body = request.json
    else:
        body = None
    if body == None:
        return Response(status=204)
    
    password = body.get("password")
    username = body.get("username")
    if password == None or username == None:
        return Response(status=206)
    # Checking if password matches
    hash = static.user_controller.select_user_password(password)

    if hash == None:
        return Response(status=400)
    if not create_hash(password) == hash:
        return Response(status=401)     # When wrong password passed
    
    validity = datetime.now() + static.jwt_validity_span

    token = jwt.encode(
        {"username":username, "validity": validity.__str__(), "rnd_payload":static.jwt_random_payload},
        static.jwt_secret, algorithm=static.jwt_algorithm
        ) 
    return Response(status=200, mimetype="application/json", response=json.dumps({"token" : token}))



@app.route("/api/addUser", methods=["POST"])
def add_user():
    # Loading json body
    if request.headers.get("Content-Type") == "application/json":
        body = request.json
    else:
        return Response(status=204)
    password = body.get("password")
    username = body.get("username")
    if username == None or password == None:
        return Response(status=206)
    
    if static.user_controller.select_user_password(username):       # Kontrola jestli už uživatel se stejným jménem neexistuje
        return Response(status=400)
    hash = create_hash(password)
    static.user_controller.add_user(username, hash)
    return Response(status=200)


