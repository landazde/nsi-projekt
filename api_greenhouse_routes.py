from flask import request, Response, redirect
from flask import Blueprint
import json
import static
import jwt
import hash


app_greenhouse = Blueprint("api_greenhouse", __name__)


@app_greenhouse.route("/api/addGreenhouse", methods=["POST"])
def add_greenhouse():
    try:
        token = request.args.get("token")
    except:
        return Response(status=206)
    
    if request.headers.get("Content-Type") == "application/json":
        body = request.json
    else:
        return Response(status=204)
    
    payload = jwt.decode(token, static.jwt_secret, algorithms=static.jwt_algorithm)
    username = payload.get("username")
    validity = payload.get("validity")
    random_payload = payload.get("rnd_payload")
    if username == None or validity == None or random_payload == None:
        return redirect("/login", code=302)
    
    greenhouse_name = body.get("greenhouse_name")
    description = body.get("description")
    if greenhouse_name == None or description == None or token == None:
        return Response(status=206)
    
    device_token = hash.create_greenhouse_token()
    greenhouse_id = static.greenhouse_controller.add(greenhouse_name, description, username, device_token)
    static.user_controller.add_greenhouse(username, greenhouse_id)
    return Response(status=200, mimetype="application/json", response=json.dumps({"id" : greenhouse_id}))


@app_greenhouse.route("/api/selectallGreenhouses", methods=["GET"])
def select_all_greenhouses():
    try:
        token = request.args.get("token")
    except:
        return Response(status=206)
    
    payload = jwt.decode(token, static.jwt_secret, algorithms=static.jwt_algorithm)
    username = payload.get("username")
    validity = payload.get("validity")
    random_payload = payload.get("rnd_payload")
    if username == None or validity == None or random_payload == None:
        return redirect("/login", code=302)
    
    greenhouses = static.greenhouse_controller.select_all_by_username(username)
    return Response(status=200, mimetype="application/json", response=json.dumps(greenhouses))


watering_topic = "watering"

@app_greenhouse.route("/api/watering")
def watering():
    """
    Funkce zalévání
    """
    try:
        token = request.args["token"]
        greenhouseId = request.args["greenhouseId"]
    except:
        return Response(status=206)
    
    payload = jwt.decode(token, static.jwt_secret, algorithms=static.jwt_algorithm)
    username = payload.get("username")
    validity = payload.get("validity")
    random_payload = payload.get("rnd_payload")
    if username == None or validity == None or random_payload == None:
        return redirect("/login", code=302)
    
    static.mqtt.publish(watering_topic+"/"+str(greenhouseId),"start")
    return Response(status=200)


@app_greenhouse.route("/api/generateDeviceToken")
def generate_device_token():
    """
    Generuje nový token pro posílání dat z Raspberry Pi Pico W nebo jiné platformy
    """
    try:
        token = request.args["token"]
        greenhouseId = request.args["greenhouseId"]
    except:
        return Response(status=206)
    
    payload = jwt.decode(token, static.jwt_secret, algorithms=static.jwt_algorithm)
    username = payload.get("username")
    validity = payload.get("validity")
    random_payload = payload.get("rnd_payload")
    if username == None or validity == None or random_payload == None:
        return redirect("/login", code=302)
    
    greenhouse = static.greenhouse_controller.select_greenhouse_by_id(greenhouseId)
    if greenhouse[0] == None:
        return Response(status=404)
    if not username == greenhouse[0][3]:
        return Response(status=400)
    
    greenhouse_token = hash.create_greenhouse_token()
    static.greenhouse_controller.update_device_token(greenhouseId, greenhouse_token)
    return Response(status=200, mimetype="application/json", response=json.dumps({"greenhouse_token" : greenhouse_token}))


    


