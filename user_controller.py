import sqlite3
import threading


class UserController:
    file: str
    connection: sqlite3.Connection
    cursor: sqlite3.Cursor
    lock = threading.Lock()


    tablename = "userTable"

    def __init__(self, file: str):
        self.file = file
        self.connection = sqlite3.connect(file, check_same_thread=False)
        self.cursor = self.connection.cursor()
        self._create_table_if_not_exists()



    def _create_table_if_not_exists(self) -> None:
        query = f"""
        CREATE TABLE IF NOT EXISTS {self.tablename}(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username TEXT NOT NULL,
            password TEXT NOT NULL,
            greenhouses TEXT
        )"""

        self.lock.acquire()

        self.cursor.execute(query)
        self.connection.commit()

        self.lock.release()



    def add_user(self, username: str, password: str) -> None:
        query = f"""
        INSERT INTO {self.tablename} (username,password,greenhouses)
        VALUES (?,?,"");
        """
        self.lock.acquire()

        self.cursor.execute(query,(username, password))
        self.connection.commit()

        self.lock.release()


    def add_greenhouse(self, username: str, greenhouse_id: int) -> None:
        query = f"""
        SELECT greenhouses FROM {self.tablename}
        WHERE username == ?;
        """
        self.lock.acquire()

        self.cursor.execute(query,[username])
        ids = self.cursor.fetchone()[0]

        if len(ids) > 0:
            ids = ids + ";" + str(greenhouse_id)
        else:
            ids = str(greenhouse_id)
  
        query = f"""
        UPDATE {self.tablename} SET greenhouses = ?
        WHERE username = ?
        """

        self.cursor.execute(query,(ids,username))
        self.connection.commit()

        self.lock.release()


    def select_greenhouses(self, username: str) -> list:
        query = f"""
        SELECT greenhouses FROM {self.tablename}
        WHERE username == ?;
        """
        self.lock.acquire()

        self.cursor.execute(query,[username])
        ids = self.cursor.fetchone()[0]

        self.lock.release()

        return ids.split(";")


    def select_user_password(self, username: str) -> str:
        query = f"""
        SELECT password FROM {self.tablename}
        WHERE username == ?;
        """
        self.lock.acquire()

        self.cursor.execute(query,[username])
        hash = self.cursor.fetchall()

        self.lock.release()

        if len(hash) > 0:
            return hash[0][0]
        return None
    


    def close_connection(self):
        self.lock.acquire()
        self.connection.close()
        self.lock.release()
