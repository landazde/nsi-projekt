from flask import Flask, request, render_template, redirect, Response
import json
from api_routes import app as api
from api_greenhouse_routes import app_greenhouse as api_greenhouse
import static
import jwt
from flask_cors import CORS
from datetime import datetime
import threading
import serial
from flask_mqtt import Mqtt
from flask_socketio import SocketIO
import sockets


app = Flask(__name__)
app.register_blueprint(api)
app.register_blueprint(api_greenhouse)
CORS(app)
websockets = SocketIO(app)


@app.route('/signUp')
def signUp():
    return render_template("signUp-form.html")


@app.route('/login')
def login():
    return render_template("login-form.html")


# Form with all greenhouses
@app.route("/greenhouses")
def greenhouses():
    try:
        token = request.args.get("token")
    except:
        pass

    if token == None:
        return redirect("/login", code=302)
    
    payload = jwt.decode(token, static.jwt_secret, algorithms=static.jwt_algorithm)
    
    username = payload.get("username")
    validity = payload.get("validity")
    random_payload = payload.get("rnd_payload")
    if username == None or validity == None or random_payload == None:
        return redirect("/login", code=302)
    
    if datetime.strptime(validity, '%Y-%m-%d %H:%M:%S.%f') < datetime.now():   # Redirection after token expiration
        return redirect("/login", code=302)
    
    all_greenhouses = static.user_controller.select_greenhouses(username=username)
    return render_template(
        "greenhouses.html",
        username=username,
        last_values=json.dumps(all_greenhouses)
        )


@app.route('/dashboard', methods=["GET"])
def dashboard():
    # Loading json body
    try:
        token = request.args.get("token")
        greenhouseId = int(request.args.get("greenhouseId"))
    except:
        return redirect("/login", code=302)

    if token == None:
        return redirect("/login", code=302)
    
    payload = jwt.decode(token, static.jwt_secret, algorithms=static.jwt_algorithm)
    
    keys = ["timestamp","temp °C","pH","moisture %"]
    username = payload.get("username")
    validity = payload.get("validity")
    random_payload = payload.get("rnd_payload")
    if username == None or validity == None or random_payload == None:
        return redirect("/login", code=302)
    
    if datetime.strptime(validity, '%Y-%m-%d %H:%M:%S.%f') < datetime.now():   # Redirection after token expiration
        return redirect("/login", code=302)
    
    result = static.get_data_controller(username, greenhouseId)
    if isinstance(result, Response):
        return result

    values_to_send = result.select_x_last(10)
    greenhouse = static.greenhouse_controller.select_greenhouse_by_id(greenhouseId)
    return render_template(
        "dashboard.html",
        username=username,
        last_values=json.dumps(values_to_send),
        length=len(values_to_send),
        keys=keys, 
        greenhouseId=greenhouseId,
        greenhouse_name=greenhouse[0][1],
        greenhouse_token=greenhouse[0][4]
        )



def serial_listener():
    try:
        ser = serial.Serial('COM3', 115200)
        ser.flushInput()

        while True:
            data = ser.readline().decode().split(";")
            result = static.get_data_controller(data[0], data[1])
            if isinstance(result, Response):
                return result
            result.add_value(data[2],data[3],data[4],data[5])
            print("inserted " + data[1])

    except:
        print("Unable to open COM port")


app.config['MQTT_BROKER_URL'] = 'broker.mqtt.cool'
app.config['MQTT_BROKER_PORT'] = 1883
mqtt = Mqtt(app)
static.mqtt = mqtt      # uložení mezi statická data aplikace

mqtt_topic = "meassurements"

@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    mqtt.subscribe(mqtt_topic)
 

@mqtt.on_message()
def subscribe_process_data(client, userData, message):
    message = message.payload.decode().split(";")
    if not static.greenhouse_controller.select_token(message[1]):
        return
    if not static.greenhouse_controller.select_token(message[1])[0][0] == message[5]:
        return
    result = static.get_data_controller(message[0], message[1])
    if isinstance(result, Response):
        return result
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    result.add_value(timestamp,message[2],message[3],message[4])
    sockets.add_value(int(message[1]), timestamp, message[2])
    print("inserted " + message[1])



if __name__ == "__main__":
    com_listener = threading.Thread(target=serial_listener)
    com_listener.start()
    websockets_thread = threading.Thread(target=sockets.run)
    websockets_thread.start()
    app.run(host='0.0.0.0', port=3000, debug=True, use_reloader=False)

