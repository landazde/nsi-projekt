# Semestrální práce

Webová aplikace na správu skleníků. V aplikaci je nejdříve potřeba se zaregistrovat a přihlásit nebo lze využít již vytvořený účet se jménem test a heslem test. Po přihlášení se zobrazí seznam skleníků, které má uživatel pod svou správou, může je přidávat a kliknutím na některý ze skleníků se dostane na stránku, kde je realtime graf s obsahující teplotu skleníku. Pod grafem je vypsaná poslední naměřená hodnota a v dalším řádku pod tím se nachází unikátní token pro možnost posílání dat ze skleníku pomocí Raspberry Pi Pico W přes MQTT. Token slouží k oveření uživatele, že data posílaná přes MQTT jsou skutečně jeho. Tento token lze červeným tlačítkem ve webovém rozhraní vygenerovat nový. O řádek níž je funkce zalévání, fungující také přes MQTT, kde se pošle zpráva do topicu watering/id_skleníku a v reakci na ní Raspberry Pi Pico W rozsvítí na chvilku LED diodu a sepne pin GPIO 0. Stav pinu se po chvilce zase zresetuje zpátky.
Na Raspberry Pi je nakonfigurované posílání dat v 5 sekundových intervalech. V rámci simulace meření se generují náhodné hodnoty pro pH, vlhkost, teplotu a čas. Tyto hodnoty jsou poté spojeny do textového řetězce společně s tokenem a id skleníku a jsou odděleny středníkem a jsou poslány přes MQTT do fask aplikace. Zde je oveřen původ dat přes token, který dorazil s daty. Poté jsou data uložena do databáze. Data jsou také uložena do slovníku, ze kterého si data poté převezme smyčka běžícího websocketového připojení k danému skleníku, Websocket se automaticky otevře pokud si uživatel zobrazí realtime graf. Pokud smyčka obsluhy websocketu zjistí že daná uživatel je registrován pro realtime data, tak mu je odešle. 
Na frontendu jsou streamováná data zpracována událostí socket.onmessage v rámci, které dochází k přidávání dat do grafu.
Ve spodní části stránky najdeme textový vstup pro mazání starých dat a zobrazení určitého počtu dat v tabulce níže. Do navigační lišty jsem umístil tlačítko pro prokliknutí se na seznam skleníků a pro odhlášení.

## Zdrojový kód

Zdrojový kód na raspberry pi pico w je ve složce raspberry_pi.<br>
<b>api_greenhouse_routes.py</b> - obsahuje endpointy pro práci se skleníky z přehledu skleníků.<br>
<b>api_routes.py</b> - endpointy pro stránku s realtime grafem.<br>
<b>data_controller.py</b> - třída pro práci s naměřenými daty.<br>
<b>greenhouse_controller</b> - databázový kontroler pro práci s tabulkou obsahující všechny skleníky.<br>
<b>hash.py</b> - obsahuje hašovací funkce pro heslo uživatele a token raspberry pi pico w.<br>
<b>main.py</b> - smyčka aplikace a spuštění vlákna pro realtimové posílání dat na frontend. Obdahuje endpointy, které vrací html formuláře.<br>
<b>sockets.py</b> - smyčka websocket serveru a obsluha připojených uživatelů k realtime grafu. Nejdříve ověří uživatele přes jeho jwt token a poté mu posílá meřená data, pokud jsou k dispozici.<br>
<b>static.py</b> - instance databázových kontrolerů a metody jejich obsluhy.<br>
<b>user_controller.py</b> - databázový kontroler pro práci s uživatelskými daty.<br>




### Přihlašovací formulář.

![Přihlašovací formulář.](./images/login_form.png)

### Registrační formulář.

![Registrační formulář.](./images/signup_form.png)

### Seznam skleníků.

![Seznam skleníků.](./images/greenhouses.png)

### Stránka s realtime grafem, zaléváním, tokenem pro meření dat a tabulkou hodnot.

![Stránka s realtime grafem, zaléváním, tokenem pro meření dat a tabulkou hodnot.](./images/dashboard.png)
