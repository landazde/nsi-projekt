import asyncio
import websockets
import threading
import jwt
import static


greenhouses = dict()
lock = threading.Lock()


def add_value(greenhouseId: int, timestamp: str, temp: str):
    lock.acquire()
    if not greenhouses.get(greenhouseId):
        greenhouses[greenhouseId] = [timestamp + ";" + temp]
    lock.release()

async def handler(websocket):
    message = await websocket.recv()
    try:
        values = message.split(";")
        token = values[1]
        greenhouseId = int(values[0])
    except:
        return
    
    # Autentifikace uživatele
    payload = jwt.decode(token, static.jwt_secret, algorithms=static.jwt_algorithm)
    username = payload.get("username")
    validity = payload.get("validity")
    random_payload = payload.get("rnd_payload")
    if username == None or validity == None or random_payload == None:
        return
    # Oveření že uživatel má přístup do daného skleníku
    greenhouse = static.greenhouse_controller.select_greenhouse_by_id(greenhouseId)
    if not greenhouse[0][3] == username:
        return
    
    lock.acquire()
    greenhouses[greenhouseId] = list()
    lock.release()
    
    while True:
        try:
            lock.acquire()
            values = greenhouses[greenhouseId]

            if len(values) > 0:
                for v in values:
                    await websocket.send(v)
                    print(v)
                    
            greenhouses[greenhouseId].clear()
            lock.release()
        except websockets.ConnectionClosedOK:
            break
        await asyncio.sleep(0.1)
    
    lock.acquire()
    del greenhouses[greenhouseId]
    lock.release()

    


async def main():
    async with websockets.serve(handler, "", 8001):
        await asyncio.Future()


def run():
    asyncio.run(main())

"""
if __name__ == "__main__":
    run()
"""

