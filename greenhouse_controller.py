import sqlite3
import threading


class GreemhouseController:
    file: str
    connection: sqlite3.Connection
    cursor: sqlite3.Cursor
    lock = threading.Lock()

    tablename: str


    def __init__(self, file: str):
        self.file = file
        self.connection = sqlite3.connect(file, check_same_thread=False)
        self.cursor = self.connection.cursor()
        self.tablename = "greenhouse"
        self._create_table_if_not_exists()


    def _create_table_if_not_exists(self) -> None:
        query = f"""
        CREATE TABLE IF NOT EXISTS {self.tablename} (
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            description REAL NOT NULL,
            username TEXT NOT NULL,
            token TEXT NOT NULL
        );"""

        self.lock.acquire()

        self.cursor.execute(query)
        self.connection.commit()

        self.lock.release()


    def add(self, name: str, descrpiton: str, username: str, token: str) -> int:
        query = f"""
        INSERT INTO {self.tablename} (name,description,username,token)
        VALUES (?,?,?,?);
        """
        print(self.tablename)
        self.lock.acquire()

        self.cursor.execute(query,[name, descrpiton, username, token])
        self.connection.commit()

        id = self.cursor.execute(f"SELECT id FROM {self.tablename} ORDER BY id DESC LIMIT 1").fetchone()[0]

        self.lock.release()
        return id
    

    def select_all_by_username(self, username: str):
        query = f"""
        SELECT * FROM {self.tablename}
        WHERE username == ?;
        """
        self.lock.acquire()

        self.cursor.execute(query,[username])
        greenhouses = self.cursor.fetchall()

        self.lock.release()

        return greenhouses
    

    def select_greenhouse_by_id(self, greenhouseId: int):
        query = f"""
        SELECT * FROM {self.tablename}
        WHERE id == ?;
        """
        self.lock.acquire()

        self.cursor.execute(query,[greenhouseId])
        greenhouse = self.cursor.fetchall()

        self.lock.release()

        return greenhouse
    
    def update_device_token(self, greenhouseId: int, new_token: str):
        query = f"""
        UPDATE {self.tablename} SET token = ?
        WHERE id = ?
        """
        self.lock.acquire()
        self.cursor.execute(query,[new_token, greenhouseId])
        self.lock.release()

    
    def select_token(self, greenhouseId: int):
        query = f"""
        SELECT token FROM {self.tablename}
        WHERE id == ?;
        """
        self.lock.acquire()

        self.cursor.execute(query,[greenhouseId])
        token = self.cursor.fetchall()

        self.lock.release()

        return token

