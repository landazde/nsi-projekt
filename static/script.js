function tableInsertRows(meassurements){
    var table = document.getElementById("table-values");
    for(let i = 0; i < meassurements.length; i++){
        var row = table.insertRow();
        var col1 = row.insertCell(0);
        var col2 = row.insertCell(1);
        var col3 = row.insertCell(2);
        var col4 = row.insertCell(3);
        col1.innerHTML = meassurements[i][0];
        col2.innerHTML = meassurements[i][1];
        col3.innerHTML = meassurements[i][2];
        col4.innerHTML = meassurements[i][3];
    }
    var lastValueLabel = document.getElementById("last-value");
    lastValueLabel.innerHTML = "Poslední změřená hodnota: " + JSON.stringify(meassurements[0]);
}

function tableDeleteAllRows(){
    var table = document.getElementById("table-values");
    while(table.rows.length > 1){
        table.deleteRow(1);
    }
}

