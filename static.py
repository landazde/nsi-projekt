from user_controller import UserController
from datetime import timedelta
from data_controller import DataController
from flask import redirect, Response
from greenhouse_controller import GreemhouseController
import threading



lock = threading.Lock()

database = "./database/database.db"
user_controller = UserController(database)
data_controllers = dict()
greenhouse_controller = GreemhouseController(database)

mqtt = None     # Variable for mqtt client, passed from main.py


jwt_secret = "8ék08kl§!opqyeashl069148"     # random payload for stronger jwt
jwt_validity_span = timedelta(hours=6)      # validity of json web token
jwt_algorithm = "HS256"
jwt_random_payload = "rndpayload13."



def __add_data_controller(username : str, greenhouseId: int):
    result = user_controller.select_greenhouses(username)
    exists = False
    for item in result:
        if item == str(greenhouseId):
            exists = True
            break

    if not exists:
        return Response(status=404)
    controller = DataController(database, username, greenhouseId)
    user_data_controllers = data_controllers.get(username)

    lock.acquire()
    if user_data_controllers == None:
        data_controllers[username] = [controller]
    else:
        data_controllers[username].append(controller)
    lock.release()

    return controller


def get_data_controller(username: str, greenhouseId: int):
    result = data_controllers.get(username)
    if result:
        for item in result:
            if item.greenhouseId == greenhouseId:
                return item
    return __add_data_controller(username, greenhouseId)

